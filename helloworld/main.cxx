#include <iostream>

#include "yell.hxx"

int
main(int argc, char** argv) {
	std::cout << yell("hello world") << std::endl;

	return 0;
}

