# My personal CMake playground to learn some twists

I thought I'd share my process of learning the nifty little details of CMake
so others could profit from it.

## Building the project

```
mkdir Release
cd Release
cmake ..
```
