#include <algorithm>
#include <string>

#include "yell.hxx"

std::string yell(const std::string& str) {
	std::string out = str;
	std::transform(out.begin(), out.end(), out.begin(), ::toupper);
	return out;
}

