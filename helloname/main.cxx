#include <iostream>

#include "yell.hxx"

int
main(int argc, char** argv) {
	if(argc != 2) {
		std::cerr << "Must supply [your name] as argument!" << std::endl;
		return 1;
	}

	std::cout << "Hello " << yell(argv[1]) << std::endl;

	return 0;
}

